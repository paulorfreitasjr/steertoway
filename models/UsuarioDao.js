function UsuarioDao(){ }

UsuarioDao.prototype.logar = function(usuario, callback){
    var db = require('./connectionFactory')
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection')

    Users.find({}).lean().exec(
    function (e, docs) {
        if(e != null){
            console.log('Erro: ' + e)
            callback(null, { 'Erro' : e})
        } else{
            console.log('Docs: ' + docs)
            callback(null, docs)
        }
    });
}

UsuarioDao.prototype.novo = function(json, callback){
    var db = require('./connectionFactory');
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');

    var user = new Users(
        { email: json.email, 
            senha: json.senha,
            nome: json.nome,
            cpf: json.cpf,
            datanasc: json.datanasc,
            telefone: json.telefone,
            genero: json.genero,
            endereco: [{
                cidade: json.endereco.cidade,
                estado: json.endereco.estado,
                rua: json.endereco.rua,
                numero: json.endereco.numero,
                complemento: json.endereco.complemento
            }]
        });
    user.save(function (err) {
        if (err) {
            console.log('Erro: ' + err.message)
            callback(null, "Erro: " + err.message)
        }
        else 
            callback(null, 'Usuário registrado com sucesso!')
    });
}

UsuarioDao.prototype.alterarSenha = function(json, callback){
    var db = require('./connectionFactory');
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');

    Users.findOne({email: json.email, senha: json.senha}, function (err, doc){
        if(err){
            console.log('Erro: ' + err.message)
            callback(null, 'Erro: ' + err.message)
        }

        doc.senha = json.novasenha
        doc.save()
        callback(null, 'Senha alterada com sucesso.')
    })
}

UsuarioDao.prototype.buscar = function(email, callback){
    var db = require('./connectionFactory');
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');

    Users.findOne({ email: email }, function (err, doc){
        if(err){
            console.log('Erro: ' + err.message)
            callback(null, 'Erro: ' + err.message)
        }

        callback(null, doc)
    })
}

UsuarioDao.prototype.atualizar = function(json, callback){
    var db = require('./connectionFactory');
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');

    var user = new Users(
        { email: json.email, 
            senha: json.senha,
            nome: json.nome,
            cpf: json.cpf,
            idade: json.idade,
            telefone: json.telefone,
            genero: json.genero,
            endereco: [{
                cidade: json.endereco.cidade,
                estado: json.endereco.estado,
                rua: json.endereco.rua,
                numero: json.endereco.numero,
                complemento: json.endereco.complemento
            }]
        });

    Users.findOne({ email: json.email }, function (err, doc){
        if(err){
            console.log('Erro: ' + err.message)
            callback(null, 'Erro: ' + err.message)
        }

        doc = user
        doc.save()
        callback(null, 'Perfil atualizado com sucesso.')
    })
}

module.exports = function(){ return UsuarioDao }