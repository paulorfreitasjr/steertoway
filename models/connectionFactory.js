var mongoose = require('Mongoose')

mongoose.connect('mongodb://localhost:27017/steertoway')

var enderecoSchema = new mongoose.Schema({
    cidade: String,
    estado: String,
    rua: String,
    numero: Number,
    complemento: String
}, { collection: 'addresscollection'})

var userSchema = new mongoose.Schema({
    email: String,
    senha: String,
    nome: String,
    cpf: String,
    datanasc: Date,
    telefone: String,
    genero: String,
    endereco: [enderecoSchema]
}, { collection: 'usercollection' })

var beaconSchema = new mongoose.Schema({
    id: String,
    codename: String,
    endereco: [enderecoSchema]
}, {collection: 'beaconcollection'})

var sugestaoSchema = new mongoose.Schema({
    login: String,
    descricao: String,
    tipo_sugestao: String,
    endereco: [enderecoSchema]
}, {collection: 'suggestionschema'})

console.log('collections identificadas')

module.exports = { Mongoose: mongoose, UserSchema: userSchema, BeaconSchema: beaconSchema }