module.exports = function(app) {
    app.get('/api/beacon', function(req, res) {
        var connection = app.models.connectionFactory()
        var beaconDao = new app.models.BeaconDao(connection)

        beaconDao.listar(function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
     });

    app.get('/api/beacon/buscar', function(req, res) {
        req.assert('codigo').notEmpty().isInt()

        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var codigo = req.get('codigo')
        var connection = app.models.connectionFactory()
        var beaconDao = new app.models.BeaconDao(connection)

        beaconDao.buscar(codigo, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.get('/api/beacon/pesquisar', function(req, res) {
        req.assert('UUID').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }
        
        var uuid = req.get('UUID')
        var connection = app.models.connectionFactory()
        var beaconDao = new app.models.BeaconDao(connection)

        beaconDao.pesquisar(uuid, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.post('/api/beacon/novo', function(req, res){
        req.assert('uuid').notEmpty()
        req.assert('descricao').notEmpty()
        req.assert('endereco_id').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var endereco = req.body
        var connection = app.models.connectionFactory()
        var beaconDao = new app.models.BeaconDao(connection)

        beaconDao.salvar(endereco, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                endereco.codigo = resultado.insertId
                res.status(200).json(endereco)
            }
        });
    });

    app.delete('/api/beacon/deletar', function(req, res){
        req.assert('codigo').notEmpty().isInt()
        
        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var codigo = req.get('codigo')
        var connection = app.models.connectionFactory()
        var beaconDao = new app.models.BeaconDao(connection)

        beaconDao.deletar(codigo, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).send("Id: " + resultado.insertId)
            }
        });
    });
}