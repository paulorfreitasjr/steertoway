module.exports = function(app) {
    app.get('/api/sugestao/', function(req, res) {
        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.listar(function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.get('/api/sugestao/naoaprovados', function(req, res) {
        req.assert('codigo').notEmpty().isInt()

        var erros = req.validationErrors()
        
        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }
        
        var codigo = req.get('codigo')

        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.naoaprovados(function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.get('/api/sugestao/aprovados', function(req, res) {
        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.aprovados(function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.get('/api/sugestao/buscar', function(req, res) {
        req.assert('codigo').notEmpty().isInt()

        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var codigo = req.get('codigo')
        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.buscar(codigo, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        });
    });

    app.post('/api/sugestao/novo', function(req, res){
        req.assert('logradouro').notEmpty()
        req.assert('bairro').notEmpty()
        req.assert('complemento').notEmpty()
        req.assert('cidade').notEmpty()
        req.assert('codigo_estado').notEmpty().isInt()

        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var endereco = req.body
        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.salvar(endereco, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                endereco.codigo = resultado.insertId
                res.status(200).json(endereco)
            }
        });
    });

    app.delete('/api/sugestao/deletar', function(req, res){
        req.assert('codigo').notEmpty().isInt()
        
        var erros = req.validationErrors()

        if(erros){
            console.log('Erros de validacao encontrados.\n' + erros)
            res.status(400).send(erros)
        }

        var codigo = req.get('codigo')
        var connection = app.models.connectionFactory()
        var sugestaoDao = new app.models.SugestaoDao(connection)

        sugestaoDao.deletar(codigo, function(erro, resultado){
            if(erro){
                console.log(erro)
                res.status(500).send(erro)
            } else {
                res.status(200).send("Id: " + resultado.insertId)
            }
        });
    });
}