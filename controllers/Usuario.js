module.exports = function(app) {
    app.get('/api/login', function(req, res) {
        req.assert('email').notEmpty()
        req.assert('senha').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            res.status(400).send(erros)
        }

        var usuario = req.body
        var usuarioDao = new app.models.UsuarioDao()
        usuarioDao.logar(usuario, function(erro, resultado){
            if(erro){
                res.status(500).send(erro)
            } else {
                res.status(200).json(resultado)
            }
        })
    })

    app.post('/api/usuario', function(req, res){
        req.assert('email').notEmpty()
        req.assert('senha').notEmpty()
        req.assert('nome').notEmpty()
        req.assert('telefone').notEmpty()
        req.assert('genero').notEmpty()
        req.assert('endereco').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            res.status(400).send(erros)
        }

        var usuario = req.body
        var usuarioDao = new app.models.UsuarioDao()

        usuarioDao.inserir(usuario, function(erro, resultado){
            if(erro){
                res.status(500).send(erro)
            } else {
                usuario.codigo = resultado.insertId
                res.status(200).send(usuario)
            }
        })
    })

    app.put('/api/usuario/alterarSenha', function(req, res){
        req.assert('email').notEmpty()
        req.assert('senha').notEmpty()
        req.assert('novasenha').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            res.status(400).send(erros)
        }

        var json = req.body
        var usuarioDao = new app.models.UsuarioDao()

        usuarioDao.alterarSenha(json, function(erro, resultado){
            if(erro){
                res.status(500).send({'mensagem' : 'Não foi possível alterar a senha.', 'erro' : erro })
            } else {
                res.status(200).send('Senha alterada com sucesso!')
            }
        })
    })

    app.get('/api/usuario', function(req, res){
        req.assert('email').notEmpty()
        
        var erros = req.validationErrors()

        if(erros){
            res.status(400).send(erros)
        }

        var email = req.get('email')
        var usuarioDao = new app.models.UsuarioDao()

        usuarioDao.buscar(email, function(erro, usuario){
            if(erro)
                res.status(500).send(erro)
            else
                res.status(200).send(usuario)
        })
    })

    app.post('/api/usuario/inserirDados', function(req, res){
        req.assert('nome').notEmpty()
        req.assert('telefone').notEmpty()
        req.assert('genero').notEmpty()
        req.assert('cpf').notEmpty()
        req.assert('endereco').notEmpty()

        var erros = req.validationErrors()

        if(erros){
            res.status(400).send(erros)
        }

        var usuario = req.body
        var usuarioDao = new app.models.UsuarioDao()

        usuarioDao.novo(categoria, function(erro, resultado){
            if(erro){
                res.status(500).send(erro)
            } else {
                res.status(200).send(resultado)
            }
        })
    })

    app.put('/api/usuario/atualizarDados', function(req, res){
        req.assert('nome').notEmpty()
        req.assert('telefone').notEmpty()
        req.assert('genero').notEmpty()
        req.assert('cpf').notEmpty()
        req.assert('rg').notEmpty()
        req.assert('codigo_endereco').notEmpty().isInt()
        req.assert('codigo').notEmpty().isInt()

        var erros = req.validationErrors()
        
        if(erros){
            res.status(400).send(erros)
        }

        var pessoa = req.body
        var connection = app.models.connectionFactory()
        var usuarioDao = new app.models.UsuarioDao(connection)

        usuarioDao.atualizarDados(pessoa, function(erro, resultado){
            if(erro){
                res.status(500).send(erro)
            } else {
                res.status(200).send(resultado)
            }
        })
    })
}