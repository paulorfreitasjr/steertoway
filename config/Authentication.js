var jwt = require('jsonwebtoken')
var passToken = 'tokenmaroto'

function Authentication(usuario, senha) {
    this.usuario = usuario
    this.senha = senha
}

Authentication.prototype.autenticar = function(callback){
    var token = jwt.sign({ usuario: this.usuario, exp: Math.floor(Date.now() / 1000) + (60 * 60), }, passToken, expireIn)
    console.log(token)
    callback(token)
}

Authentication.prototype.validarToken = function(token, callback){
    try {
        var decoded = jwt.verify(token, passToken);
        Console.log('Usuario "' + decoded.usuario + ' autenticado')
        callback({ code: 200})
      } catch(err) {
        console.log(err)
        callback({ code: 401})
      }
}
